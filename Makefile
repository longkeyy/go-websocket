export PATH := $(GOPATH)/bin:$(PATH)
export GO15VENDOREXPERIMENT := 1
LDFLAGS := -s -w

all: init fmt build upxfast package

debug: init fmt build


build: go-websocket

fmt:
	go fmt ./...

init:
	mkdir -p dist
	rm -f dist/*
	go get github.com/gorilla/mux
	go get github.com/gorilla/websocket

go-websocket:
	env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "$(LDFLAGS)" -o dist/go-websocket_linux_amd64 .
	env CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags "$(LDFLAGS)" -o dist/go-websocket_windows_amd64 .
	env CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -ldflags "$(LDFLAGS)" -o dist/go-websocket_darwin_amd64 .
	#env CGO_ENABLED=0 GOOS=linux GOARCH=mips GOMIPS=softfloat go build -ldflags "$(LDFLAGS)" -o dist/go-websocket_linux_mips .


upxfast:
	upx  dist/go-websocket_linux_amd64
	upx  dist/go-websocket_windows_amd64
	upx  dist/go-websocket_darwin_amd64


upx:
	upx -9 --ultra-brute dist/go-websocket_linux_amd64
	upx -9 --ultra-brute dist/go-websocket_windows_amd64
	upx -9 --ultra-brute dist/go-websocket_darwin_amd64

package:
	rm -f go-websocket.tar.gz
	tar zcvf go-websocket.tar.gz dist/*

clean:
	rm -rf dist
