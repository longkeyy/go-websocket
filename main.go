// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/http"
	"net/url"
	"os"
	"sync"

	"os/signal"
	"time"

	"bytes"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

const authParam = "auth"

var (
	hubs = map[string]*Hub{}

	mode         = flag.String("mode", "server", "server/client/bridge/demo")
	addr         = flag.String("addr", "", "server service address")
	port         = flag.String("port", "3721", "server service port")
	auth         = flag.String("key", "", "server message key")
	path         = flag.String("path", "/ws", "server path")
	localWsAddr  = flag.String("local_ws", "ws://localhost:3721/ws/1", "bridge mode local websocket uri")
	remoteWsAddr = flag.String("remote_ws", "wss://api.huobipro.com/ws", "client/bridge mode remote websocket uri")
	remoteWsZip  = flag.Bool("remote_zip", true, "unzip message")
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

var p sync.Mutex

func ws(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	s := params["ch"]
	hub, present := hubs[s]
	if !present {
		p.Lock()
		hub = newHub()
		hubs[s] = hub
		p.Unlock()
		go hub.run()
		log.Printf("%s create new channel %s", r.RemoteAddr, s)
	}
	serveWs(hub, w, r)
	log.Printf("%s join channel %s", r.RemoteAddr, s)
}

func server() {
	router := mux.NewRouter()
	router.HandleFunc("/", home)
	router.HandleFunc(*path+"/{ch}", ws)
	err := http.ListenAndServe(*addr+":"+*port, router)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func client() {
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u, _ := url.Parse(*remoteWsAddr)
	if *auth != "" {
		query := u.Query()
		query.Set(authParam, *auth)
		u.RawQuery = query.Encode()
	}
	log.Printf("connecting to %s", u.String())
	websocket.DefaultDialer.EnableCompression = true
	ws, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer ws.Close()

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := ws.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}

			if *remoteWsZip {
				var buf bytes.Buffer
				err = gunzipWrite(&buf, message)
				if err != nil {
					return
				}
				log.Printf("recv: %s", buf.String())
			} else {
				log.Printf("recv: %s", message)
			}
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	//go func() {
	//	ws.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.kline.1min\",\"id\": \"id10\"}"))
	//	ws.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.depth.step5\",\"id\": \"id10\"}"))
	//	ws.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.trade.detail\",\"id\": \"id10\"}"))
	//}()
	for {
		select {
		case <-done:
			log.Println("server close")
			return

		case t := <-ticker.C:
			err := ws.WriteMessage(websocket.TextMessage, []byte(t.String()+"\r\n"+"haha"))
			if err != nil {
				log.Println("write:", err)
				return
			}
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := ws.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}

func main() {
	flag.Parse()
	switch *mode {
	case "server":
		server()
		break
	case "client":
		client()
		break
	case "bridge":
		bridge()
		break
	case "demo":
		go server()
		bridge()
	}
}
