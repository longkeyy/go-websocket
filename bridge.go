package main

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
)

func huobiHandMessage(remoteWs *websocket.Conn, localWs *websocket.Conn) {
	_, message, err := remoteWs.ReadMessage()
	if err != nil {
		log.Println("read:", err)
		return
	}

	//resultInterface := interface {}
	var resultInterface map[string]interface{}
	if *remoteWsZip {
		var buf bytes.Buffer
		err = gunzipWrite(&buf, message)
		if err == nil {
			message = buf.Bytes()
		}
	}
	decoder := json.NewDecoder(bytes.NewBuffer(message))
	decoder.UseNumber()
	decoder.Decode(&resultInterface)
	if v, ok := resultInterface["ping"]; ok {
		//remoteWs.WriteMessage(websocket.PongMessage, message)
		remoteWs.WriteMessage(websocket.TextMessage, bytes.Replace(message, []byte("ping"), []byte("pong"), -1))
		log.Printf("pong: %s", v)
	}
	localWs.WriteMessage(websocket.TextMessage, message)
}

func huobiOnConnect(remoteWs *websocket.Conn) {
	remoteWs.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.kline.1min\",\"id\": \"id10\"}"))
	remoteWs.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.depth.step5\",\"id\": \"id10\"}"))
	remoteWs.WriteMessage(websocket.TextMessage, []byte("{\"sub\": \"market.ethusdt.trade.detail\",\"id\": \"id10\"}"))
}

func bridge() {

	websocket.DefaultDialer.EnableCompression = true

	localWs, _, err := websocket.DefaultDialer.Dial(*localWsAddr, nil)

	if err != nil {
		log.Fatal("local dial:", err)
	}

	defer localWs.Close()

	remoteWs, _, err := websocket.DefaultDialer.Dial(*remoteWsAddr, nil)

	if err != nil {
		log.Fatal("remote dial:", err)
	}

	defer remoteWs.Close()

	remoteDone := make(chan struct{})
	localDone := make(chan struct{})

	go func() {
		defer close(localDone)
		for {
			_, _, err := localWs.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
		}

	}()

	go func() {
		defer close(remoteDone)
		for {
			huobiHandMessage(remoteWs, localWs)
		}
	}()

	go huobiOnConnect(remoteWs)

	for {
		select {
		case <-remoteDone:
			log.Println("remote server close")
			return
		case <-localDone:
			log.Println("local server close")
			return
		}

	}
}
